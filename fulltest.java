

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class fulltest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class fulltest
{
    /**
     * Default constructor for test class fulltest
     */
    public fulltest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    @Test
    public void fulltest1()
    {
     SY s = new SY ("2 + 3");
     Postfix p = new Postfix (s.convert());
     assertEquals(5.0,p.eval(), 0.001);
     
    }
    @Test
    public void fulltest2()
    {
     SY s = new SY ("2 * ( 3 + 4 )");
     Postfix p = new Postfix (s.convert());
     assertEquals(14.0, p.eval(), 0.001);
        
    }
}
