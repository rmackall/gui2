import java.util.Stack;

public class SY
{
    String infix;
    
    public SY(String i)
    {
        infix = i;
    }
    
    public String convert()
    {
        String[] tokens = infix.split(" ");
        String postfix = "";
        Stack<String> stack = new Stack<String>();
        
        for (int i = 0; i < tokens.length; i++)
        {
            
            
            if (tokens[i].equals("("))
            {
             stack.push(tokens[i]);    
            }
            else if (tokens[i].equals(")")) 
                {
                    while (!stack.peek().equals("("))
                        postfix = postfix + stack.pop()+ " ";
                        stack.pop();
                }
                    
                
            else if(prec(tokens[i]) == -1)
            {
                // Append token to postfix
                postfix = postfix + tokens[i] + " ";
            }
            else
            {
                if (stack.isEmpty())
                {
                    stack.push(tokens[i]);
                }
                
                else if (prec(tokens[i]) > prec(stack.peek()))
                {
                    stack.push(tokens[i]);
                }
                else
                {
                    postfix = postfix + stack.pop() + " ";
                    stack.push(tokens[i]);
                }
            }
        }
        
        while(!stack.isEmpty())
        {
            postfix = postfix + stack.pop() + " ";
        }
        
        return postfix.trim();
    }
    
    // Return precendence of operator
    // Returns 4 for parentesis,
    //   3 for exponent (^)
    //   2 for * or /
    //   1 for + or -
    //   -1 for anything else
    public int prec(String token)
    {
        switch(token)
        {
            case "sqrt":
                return 3;
            case ")":
                return 1;
            case "(":
                return 0;
            case "^":
                return 4;
            case "*":
            case "/":
                return 3;
            case "+":
            case "-":
                return 2;
            default:
                return -1;
        }
    }
}