import java.util.Stack;

public class Postfix
{
    String expr;
    Stack<Double> stack;

    public Postfix(String e)
    {
        expr = e;
        stack = new Stack<Double>();
    }

    public double eval()
    {
        String[] tokens = expr.split(" ");

        for (int i = 0; i < tokens.length; i++)
        {
            if (tokens[i].equals("+"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double sum = a + b;
                stack.push(sum);
            }
            else if (tokens[i].equals("*"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double sum = a * b;
                stack.push(sum);
            }

            else if (tokens[i].equals("-"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double sum = b - a;
                stack.push(sum);
            }

            else if (tokens[i].equals("/"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double sum = b / a;
                stack.push(sum);
            }

            else if (tokens[i].equals("^"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double sum = Math.pow(b,a);
                stack.push(sum);
            }

            else if (tokens[i].equals("sqrt"))
            {
                double a = stack.pop();
                double sum = Math.sqrt(a);
                stack.push(sum);
            }
            else
            {
                stack.push(Double.parseDouble(tokens[i]));
            }
        }

        return stack.pop();
    }
}