import acm.program.*;
import acm.graphics.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.lang.Error;
import java.lang.Exception;

public class GUI extends Program
{
    JTextField infixIn;
    JTextField infixOutput;
    JTextField totalAwnser;

   
    public GUI()
    {
        start();
        setSize(450, 250);
    }

    public void init()
    {
        // Create a canvas and add it to the window
        GCanvas canvas = new GCanvas();
        add(canvas);
        setTitle("MidTerm Calculator");

        
        Color uglyColor = new Color(102, 255, 153);
        canvas.setBackground(uglyColor);

        
        JLabel postLabel = new JLabel("Post");
        JLabel infixLabel = new JLabel("Infix");
        JLabel anwserLabel = new JLabel("Awnser");
        canvas.add(postLabel, 20, 20);
        canvas.add(infixLabel, 20, 60);
        canvas.add(anwserLabel, 20, 100);

        infixIn = new JTextField();
        infixOutput = new JTextField();
        totalAwnser = new JTextField();
        canvas.add(infixIn, 70, 20);
        canvas.add(infixOutput, 70, 60);
        canvas.add(totalAwnser, 70, 100);
        infixIn.setSize(100, 20);
        infixOutput.setSize(100, 20);
        totalAwnser.setSize(100, 20);

        JButton convertButton = new JButton("Convert");
        JButton clearButton = new JButton("Clear");
        JButton ansButton = new JButton("ANS");
        canvas.add(convertButton, 20, 125);
        canvas.add(clearButton, 120, 125);
        canvas.add(ansButton, 80, 155);

        
        JButton numb1 = new JButton("1");
        JButton numb2 = new JButton("2");
        JButton numb3 = new JButton("3");
        JButton numb4 = new JButton("4");
        JButton numb5 = new JButton("5");
        JButton numb6 = new JButton("6");
        JButton numb7 = new JButton("7");
        JButton numb8 = new JButton("8");
        JButton numb9 = new JButton("9");
        JButton numb0 = new JButton("0");
        JButton numbpower = new JButton("^");
        JButton numbrp = new JButton(")");
        JButton numblp = new JButton("(");
        JButton numbmul = new JButton("*");
        JButton numbdiv = new JButton("/");
        JButton numbmin = new JButton("-");
        JButton numbplus = new JButton("+");
        JButton numbsqrt = new JButton("sqrt");

        canvas.add(numbpower, 300, 95);
        canvas.add(numbrp, 335, 70);
        canvas.add(numblp, 300, 70);
        canvas.add(numbmul, 300, 45);
        canvas.add(numbdiv, 335, 45);
        canvas.add(numbmin, 335, 20);
        canvas.add(numbplus, 300, 20);
        canvas.add(numbsqrt, 340, 95);

        canvas.add(numb1, 175, 20);
        canvas.add(numb2, 215, 20);
        canvas.add(numb3, 255, 20);
        canvas.add(numb4, 175, 45);
        canvas.add(numb5, 215, 45);
        canvas.add(numb6, 255, 45);
        canvas.add(numb7, 175, 70);
        canvas.add(numb8, 215, 70);
        canvas.add(numb9, 255, 70);
        canvas.add(numb0, 215, 95);

        addActionListeners();
    }

    public void actionPerformed(ActionEvent e)
    {
    
        //System.out.println(e.getActionCommand());
        if (e.getActionCommand().equals("Convert"))
        {
            
            {// Get value from mphInput
                String infixStr = infixIn.getText();
                String infix = (infixStr);
                // Create Mph2Mps converter
                SY n = new SY("" + infix);

                // Call convert
                String postfix = n.convert();

                // Put result into mpsOutput
                infixOutput.setText("" + postfix);

                //Awnser 
                Postfix a = new Postfix(postfix);
                double awn = a.eval();
                totalAwnser.setText("" + awn);
            }         


        }
        else 


        if (e.getActionCommand().equals("Clear"))
        {
            infixIn.setText("");
            infixOutput.setText("");
            totalAwnser.setText("");
        }

        if (e.getActionCommand().equals("ANS"))
        {
            String infixStr2 = totalAwnser.getText();
            String infix2 = (infixStr2);
            infixIn.setText(infixStr2);

        }
        if (e.getActionCommand().equals("+"))
        {
            infixIn.setText("+");
        }
        if (e.getActionCommand().equals("-"))
        {
            infixIn.setText("-");
        }
        if (e.getActionCommand().equals("*"))
        {
            infixIn.setText("*");
        }
        if (e.getActionCommand().equals("/"))
        {
            infixIn.setText("/");
        }
        if (e.getActionCommand().equals("^"))
        {
            infixIn.setText("^");
        }
        if (e.getActionCommand().equals("("))
        {
            infixIn.setText("(");
        }
        if (e.getActionCommand().equals(")"))
        {
            infixIn.setText(")");
        }
        if (e.getActionCommand().equals("sqrt"))
        {
            infixIn.setText("sqrt");
        }    
        if (e.getActionCommand().equals("1"))
        {
            infixIn.setText("1");
        }
        if (e.getActionCommand().equals("2"))
        {
            infixIn.setText("2");
        }
        if (e.getActionCommand().equals("3"))
        {
            infixIn.setText("3");
        }
        if (e.getActionCommand().equals("4"))
        {
            infixIn.setText("4");
        }
        if (e.getActionCommand().equals("5"))
        {
            infixIn.setText("5");
        }
        if (e.getActionCommand().equals("6"))
        {
            infixIn.setText("6");
        }
        if (e.getActionCommand().equals("7"))
        {
            infixIn.setText("7");   
        }
        if (e.getActionCommand().equals("8"))
        {
            infixIn.setText("8");
        }
        if (e.getActionCommand().equals("9"))
        {
            infixIn.setText("9");
        }
        if (e.getActionCommand().equals("0"))
        {
            infixIn.setText("0");
        }
    } 
}